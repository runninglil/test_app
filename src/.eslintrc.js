module.exports = {
  root: true,
  extends: ['@byted/eslint-config-eden/react-ts'],
  plugins: ['prettier'],
  rules: {
    // eslint-disable-next-line
    'prettier/prettier': 'error',
  },
};
